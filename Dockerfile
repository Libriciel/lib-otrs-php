FROM php:7.3.5-fpm-stretch


RUN apt-get update && apt-get install -y \
    libxml2-dev \
    git \
   && rm -r /var/lib/apt/lists/*

RUN docker-php-ext-install \
    soap


RUN pecl install xdebug && docker-php-ext-enable xdebug
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php --install-dir=/usr/local/bin
RUN mv /usr/local/bin/composer.phar /usr/local/bin/composer


WORKDIR /usr/local/lib/lib-otrs-php
COPY . /usr/local/lib/lib-otrs-php


RUN cd /usr/local/lib/lib-otrs-php && composer install

