<?php

use Libriciel\LibOTRS\OTRSConfiguration;
use Libriciel\LibOTRS\OTRSFacilities;

$time = microtime(true);

require_once __DIR__."/../vendor/autoload.php";
require_once __DIR__."/../DefaultSettings.php";

$otrsConfiguration = new OTRSConfiguration();
$otrsConfiguration->url = OTRS_RPC_URL;
$otrsConfiguration->login = OTRS_USERNAME;
$otrsConfiguration->password = OTRS_PASSWORD;
$otrsConfiguration->otrs_user_id = OTRS_USER_ID;

$otrsFacilities = new OTRSFacilities($otrsConfiguration);

try {
    $result = $otrsFacilities->retrieveInfoForCustomerUser('ecollectivitesvendee@cdg85.fr');
} catch (Exception $e){
    echo "Problème lors de l'accès à OTRS : "  . $e->getMessage();
    exit(-1);
}

print_r($result);

echo sprintf("%0.3fs",microtime(true) - $time)."\n";
exit(0);

/*
 *
 * $result exemple :
 *
 * Array
(
    [2 - A présenter au prochain comité "évolution"  (action LS)] => 3
    [3 - Traité par l'interlocuteur Libriciel SCOP] => 1
    [1 - A qualifier par le responsable de pôle (action LS)] => 1
    [2 - A présenter au prochain "atelier de travail"  (action LS)] => 2
    [2 - A qualifier par le client (action client)] => 1
    [2 - En attente date du RDV fixé par l'interlocuteur Libriciel SCOP (action LS)] => 1
)

 *
 *
 */