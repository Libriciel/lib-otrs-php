<?php

namespace Libriciel\LibOTRS;

class OTRSConfiguration {

    // Exemple : https://client.libriciel.fr/otrs/rpc.pl
    public $url;

    // Exemple : soap
    public $login;

    public $password;

    public $otrs_user_id;

}