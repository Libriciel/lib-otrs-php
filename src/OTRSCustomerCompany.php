<?php

namespace Libriciel\LibOTRS;

class OTRSCustomerCompany extends OTRSObject {

	public $customerID;
	public $customerCompanyName;
	public $customerCompanyStreet;
	public $customerCompanyZIP;
	public $customerCompanyCity;
	public $customerCompanyCountry;
	public $customerCompanyURL;
	public $customerCompanyComment;
	public $validID = 0;
	public $customerCompanySugarID;
	public $customerCompanyCarnetDeTempsReste = 0;
	public $customerCompanyCarnetDeTempsTotal = 0;
	public $customerCompanyCarnetDeTempsConsomme = 0;

	protected function getMandatoryFields(){
		return array($this->customerID,$this->customerCompanyName,$this->validID);
	}
}