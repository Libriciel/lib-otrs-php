<?php

namespace Libriciel\LibOTRS;

class OTRSCustomerUser extends OTRSObject {

	public $userLogin;
	public $userTitle;
	public $userFirstname;
	public $userLastname;
	public $userEmail;
	public $userCustomerID;
	public $validID;
	public $userPhone;
	public $userFax;
	public $userMobile;
	public $userStreet;
	public $userZip;
	public $userCity;
	public $userCountry;
	public $userComment;
	public $userSugarID;

	protected function getMandatoryFields(){
		return array($this->userLogin,
			$this->userFirstname,
			$this->userLastname,
			$this->userLastname,
			$this->validID
		);
	}

}