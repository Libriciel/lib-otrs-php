<?php

namespace Libriciel\LibOTRS;

use SoapFault;

class OTRSFacilities  {

    /**
     * @var OTRSWrapper
     */
    private $otrsWrapper;

    public function __construct(OTRSConfiguration $otrsConfiguration) {
        $otrsWrapper = new OTRSWrapper($otrsConfiguration);
        $this->setOTRSWrapper($otrsWrapper);
    }

    public function setOTRSWrapper(OTRSWrapper $otrsWrapper){
        $this->otrsWrapper = $otrsWrapper;
    }
    /**
     * @param string $customerUserLogin
     * @return array
     * @throws SoapFault
     */
    public function retrieveInfoForCustomerUser(string $customerUserLogin) : array {
        $search_result = $this->otrsWrapper->searchTicket([
            'CustomerUserLogin' => $customerUserLogin,
            'StateType' => 'Open',
        ]);

        if (! $search_result){
            return [];
        }

        $result = ['states'=>[],'accounted_time'=>0];
        foreach($search_result as $ticket_id => $ticket_number){
            $ticket_info = $this->otrsWrapper->getTicketInfo($ticket_id);
            $result['states'][$ticket_info['State']] ++;
            $result['accounted_time'] += $this->otrsWrapper->getTicketAccountTime($ticket_id);
        }
        return $result;
    }




}