<?php

namespace Libriciel\LibOTRS;

use Exception;
use ReflectionException;
use SoapFault;
use SoapVar;

class OTRSWrapper {

	private $url;
	private $login;
	private $password;
	private $otrs_user_id;

    /**
     * @var SoapClientFactory
     */
	private $soapClientFactory;

	public function __construct(
	    OTRSConfiguration $otrsConfiguration
    ){
		$this->url = $otrsConfiguration->url;
		$this->login = $otrsConfiguration->login;
		$this->password = $otrsConfiguration->password;
		$this->otrs_user_id = $otrsConfiguration->otrs_user_id;
		$this->setSoapClientFatory(new SoapClientFactory());
	}

	public function setSoapClientFatory(SoapClientFactory $soapClientFactory){
	    $this->soapClientFactory = $soapClientFactory;
    }

    /**
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function getCustomerCompanyList(){
		return $this->call("CustomerCompanyObject","CustomerCompanyList",array("Search"=>"*","Limit"=>0));
	}

    /**
     * @param $customer_id
     * @return OTRSCustomerCompany|null
     * @throws ReflectionException
     * @throws SoapFault
     */
	public function getCustomerCompany($customer_id){
		$info =  $this->call("CustomerCompanyObject","CustomerCompanyGet",array("CustomerID"=>$customer_id));
		if (!$info){
			return null;
		}
		return new OTRSCustomerCompany($info);
	}

    /**
     * @param OTRSCustomerCompany $company
     * @return array|bool|mixed
     * @throws MandatoryFieldsException
     * @throws ReflectionException
     * @throws SoapFault
     */
	public function addCustomerCompany(OTRSCustomerCompany $company){
		return $this->editCustomerCompany("CustomerCompanyAdd",$company);
	}

    /**
     * @param OTRSCustomerCompany $company
     * @return array|bool|mixed
     * @throws MandatoryFieldsException
     * @throws ReflectionException
     * @throws SoapFault
     */
	public function updateCustomerCompany(OTRSCustomerCompany $company){
		return $this->editCustomerCompany("CustomerCompanyUpdate",$company);
	}

    /**
     * @param $operation
     * @param OTRSCustomerCompany $company
     * @return array|bool|mixed
     * @throws MandatoryFieldsException
     * @throws ReflectionException
     * @throws SoapFault
     */
	public function editCustomerCompany($operation,OTRSCustomerCompany $company){
		$callParam = $company->getArray();
		$callParam['UserID'] = $this->otrs_user_id;
		return $this->call("CustomerCompanyObject", $operation, $callParam);
	}

    /**
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function getCustomerUserIDList(){
		return $this->call("CustomerUserObject","CustomerUserList",array());
	}

    /**
     * @param $customer_id
     * @return OTRSCustomerUser|null
     * @throws ReflectionException
     * @throws SoapFault
     */
	public function getCustomerUser($customer_id){
		$info = $this->call("CustomerUserObject","CustomerUserDataGet",array("User" => $customer_id));
		if (! $info){
			return null;
		}
		return new OTRSCustomerUser($info);
	}

    /**
     * @param OTRSCustomerUser $customerUser
     * @return array|bool|mixed
     * @throws MandatoryFieldsException
     * @throws ReflectionException
     * @throws SoapFault
     */
	public function addCustomerUser(OTRSCustomerUser $customerUser){
		return $this->editCustomerUser("CustomerUserAdd",$customerUser);
	}

    /**
     * @param OTRSCustomerUser $customerUser
     * @return array|bool|mixed
     * @throws MandatoryFieldsException
     * @throws ReflectionException
     * @throws SoapFault
     */
	public function updateCustomerUser(OTRSCustomerUser $customerUser){
		return $this->editCustomerUser("CustomerUserUpdate",$customerUser);
	}

    /**
     * @param $operation
     * @param OTRSCustomerUser $customerUser
     * @return array|bool|mixed
     * @throws MandatoryFieldsException
     * @throws ReflectionException
     * @throws SoapFault
     */
	private function editCustomerUser($operation,OTRSCustomerUser $customerUser){
		$callParam = array("Source" => "CustomerUser", "ID"=> $customerUser->userLogin);
		$callParam = array_merge($callParam,$customerUser->getArray());
		$callParam["UserID"] = $this->otrs_user_id;
		return $this->call(
			"CustomerUserObject",
			$operation,
			$callParam);
	}

    /**
     * @param $customer_id
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function getGroup($customer_id){
		$info = $this->call(
			"CustomerGroupObject",
			"GroupMemberList",
			array(
				"UserID" => $customer_id,
				"Result"=>"HASH",
				"Type"=>"rw"
			)
		);
		if (! $info){
			return array();
		}

		return $info;
	}

    /**
     * @param $customer_id
     * @param $group_id
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function add2group($customer_id,$group_id){
		return $this->setCustomerGroup($customer_id,$group_id,1);
	}

    /**
     * @param $customer_id
     * @param $group_id
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function remove2group($customer_id,$group_id){
		return $this->setCustomerGroup($customer_id,$group_id,0);
	}

    /**
     * @param $paramName
     * @return mixed
     * @throws SoapFault
     */
	public function getConfig($paramName){
		return $this->internalCall("ConfigObject","Get",array($paramName=>""));
	}

    /**
     * @return mixed
     * @throws SoapFault
     */
	public function getCustomerGroupAlwaysGroups(){
		return $this->getConfig("CustomerGroupAlwaysGroups");
	}


    /**
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function getServiceList(){
		return $this->call("ServiceObject","ServiceList",array("UserID"=>$this->otrs_user_id));
	}

    /**
     * @param $customerUserLogin
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function getCustomerUserServiceMemberList($customerUserLogin){
		$info =  $this->call(
			"ServiceObject",
			"CustomerUserServiceMemberList",
			array(
				"CustomerUserLogin"=>$customerUserLogin,
				"Result"=>'HASH',
				"DefaultServices" => 0,
			)
		);
		if (! $info){
			return array();
		}
		return $info;
	}

    /**
     * @param $customerUserLogin
     * @param $serviceID
     * @param int $active
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function customerUserServiceMemberAdd($customerUserLogin,$serviceID,$active=1){
		return $this->call(
			"ServiceObject",
			"CustomerUserServiceMemberAdd",
			array(
				"CustomerUserLogin"=>$customerUserLogin,
				"ServiceID"=>$serviceID,
				"Active"=>$active,
				"UserID"=>$this->otrs_user_id
			)
		);
	}

    /**
     * @param $customer_id
     * @param $group_id
     * @param $has_permission
     * @return array|bool|mixed
     * @throws SoapFault
     */
	private function setCustomerGroup($customer_id,$group_id,$has_permission){
		return $this->call(
			"CustomerGroupObject",
			"GroupMemberAdd",
			array(
				"GID"=>$group_id,
				"UID"=>$customer_id,
				"Permission"=> array("rw"=>$has_permission),
				"UserID" => $this->otrs_user_id
			)
		);
	}


    /**
     * @param $itemID
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function getFAQInfo($itemID){
		return $this->call(
			"FaqObject",
			"FAQGet",
			array(
				'ItemID' => $itemID,
				'UserID' => $this->otrs_user_id,
			)
		);
	}

    /**
     * @param $info
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function faqEdit($info){
		return $this->call("FaqObject","FAQUpdate",array(
				'ItemID'=>$info['ItemID'],
				'UserID'=>$this->otrs_user_id,
				'Title'=>$info['Title'],
				'CategoryID'=>$info['CategoryID'],
				'StateID'=>$info['StateID'],
				'LanguageID' => $info['LanguageID'],
				'ContentType' => $info['ContentType'],
			)
		);
	}

    /**
     * @param $itemID
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function getFAQAttachments($itemID){
		$attachment_list =  $this->call("FaqObject","AttachmentIndex",array('ItemID'=>$itemID,
				'UserID'=>$this->otrs_user_id,
			),true
		);
		if (! $attachment_list){
			return array();
		}
		if (is_object($attachment_list) ){
			return array($attachment_list);
		}
		return $attachment_list;
	}

    /**
     * @param $itemID
     * @param $fileID
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function deleteFaqAttachment($itemID,$fileID){
		return $this->call("FaqObject","AttachmentDelete",
			array('ItemID'=>$itemID,
				'UserID'=>$this->otrs_user_id,
				'FileID' => $fileID,
			));
	}

    /**
     * @param $itemID
     * @throws Exception
     */
	public function deleteAllFaqAttachment($itemID){
		$attachment_list = $this->getFAQAttachments($itemID);
		if (! $attachment_list){
			return;
		}
		foreach($attachment_list as $file){
			$result =$this->deleteFaqAttachment($itemID,$file->FileID);
			if (! $result){
				throw new Exception("Impossible de supprimer le fichier $itemID::{$file->FileID}");
			}
		}
	}

    /**
     * @param $itemID
     * @param $filepath
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function addFaqAttachment($itemID,$filepath){
		$filename = basename($filepath);
		$content_type = mime_content_type($filepath);
		return $this->call(
			"FaqObject",
			"AttachmentAdd",
			array('ItemID'=>$itemID,
				'UserID'=>$this->otrs_user_id,
				'IsBase64Encoded'=>1,
				'Content' => new SoapVar(file_get_contents($filepath),XSD_BASE64BINARY, 'xsd:base64Binary'),
				'ContentType' => $content_type,
				'Filename'    => $filename,
				'Inline'      => 0,
			)
		);
	}

    /**
     * @param $itemID
     * @param $filepath
     * @param $title
     * @throws Exception
     */
	public function publishFAQ($itemID,$filepath,$title) {
		if (! file_exists($filepath)){
			throw new Exception("Le document $filepath n'existe pas ou n'est pas accessible");
		}
		$info = $this->getFAQInfo($itemID);
		if (!$info) {
			throw new Exception("L'article $itemID n'existe pas sur la FAQ OTRS !");
		}
		$info['Title'] = $title;
		$this->faqEdit($info);
		$this->deleteAllFaqAttachment($itemID);
		$this->addFaqAttachment($itemID,$filepath);
	}

    /**
     * @param array $critere
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function searchTicket(array $critere){
		$critere['UserID'] = $this->otrs_user_id;
		return $this->call(
			"TicketObject",
			"TicketSearch",
			$critere
		);
	}

    /**
     * @param $ticket_id
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function getTicketAccountTime($ticket_id){
		return $this->call(
			'TicketObject',
			'TicketAccountedTimeGet',
			array(
				'TicketID' => $ticket_id
			),
			true
		);
	}

    /**
     * @param $ticket_id
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function getTicketInfo($ticket_id){
		return $this->call(
			"TicketObject",
			"TicketGet",
			array(
				'UserID' => OTRS_USER_ID,
				'TicketID' => $ticket_id,
			)
		);
	}

    /**
     * @param array $queueIDs
     * @return array
     * @throws SoapFault
     */
	public function getAccountedTimePerCustomer(array $queueIDs){
		$result = array();
		$ticket_in_ct = $this->searchTicket(array('QueueIDs'=>$queueIDs));

		foreach($ticket_in_ct as $ticket_id => $id) {
			$accounted_time = $this->getTicketAccountTime($ticket_id);
			if (! $accounted_time){
				continue;
			}

			$ticket_info = $this->getTicketInfo($ticket_id);
			if (! isset($result[$ticket_info['CustomerID']])) {
				$result[$ticket_info['CustomerID']] = 0;
			}
			$result[$ticket_info['CustomerID']] += $accounted_time;
		}
		return $result;
	}


    /**
     * @param $object
     * @param $fonction
     * @param array $param
     * @return mixed
     * @throws SoapFault
     */
	public function internalCall($object, $fonction,array $param = array()){


        $stream_context = stream_context_create(
            array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false
                )
            )
        );

        $soapInitParam = 	array(
			'location' => $this->url,
			'uri' => "Core",
			'login' => $this->login,
			'password' => $this->password,
			'style' => SOAP_RPC,
			'use' => SOAP_ENCODED,
			'trace'=>1,
            "stream_context" => $stream_context
        );


		$client = $this->soapClientFactory->getInstance($soapInitParam);

		$soapCallParam = array($this->login, $this->password, $object, $fonction);
		foreach($param as $key => $value){
			$soapCallParam[] = $key;
			$soapCallParam[] = $value;
		}

        $result = $client->__soapCall("Dispatch", $soapCallParam);
		return $result;
	}

    /**
     * @param $object
     * @param $fonction
     * @param array $param
     * @param bool $return_raw
     * @return array|bool|mixed
     * @throws SoapFault
     */
	public function call($object, $fonction,array $param = array(),$return_raw = false){

		$soapResult = $this->internalCall($object,$fonction,$param);
		if (! $soapResult){

			return false;
		}


		if (is_string($soapResult) || is_int($soapResult)){
			return $soapResult;
		}

		$temp = array();
		if ( $return_raw){
			return $soapResult;
		}

		foreach ($soapResult as $name => $value) {
			$temp[] = $value;
		}

		$result = array();
		foreach ($temp as $i => $value) {
			if($i%2 == 0){
				continue;
			}
			$result[$temp[$i-1]] = $value;
		}

		return $result;
	}

}