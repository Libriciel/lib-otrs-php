<?php

namespace Libriciel\LibOTRS;

use SoapClient;
use SoapFault;

class SoapClientFactory {

    private $soapClient;

    /**
     * @param $soap_options = null
     * @return SoapClient
     * @throws SoapFault
     */
    public function getInstance(array $soap_options = null) : SoapClient {
        if (! $this->soapClient) {
            $this->soapClient = new SoapClient(
                null,
                $soap_options
            );
        }
        return $this->soapClient;
    }
}